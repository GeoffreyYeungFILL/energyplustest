from random import random
import os
import opyplus as op
import matplotlib.pyplot as plt

from math import sqrt
from typing import List, Union
from time import time
numeric = Union[int, float]


def calculate_errors(data: List[numeric], expected: List[numeric]) -> (float, float):
    """calculates Coefficient of Variation of Root Mean Square Error,
    and the Mean Bias Error"""
    if len(data) != len(expected):
        raise ValueError(
            "len of data should be the same with len of expected data")
    n = len(data)

    # note: CoV-RMS is just Root Mean Square Error, but normalized
    rms = sum([(d - e) ** 2 for (d, e) in zip(data, expected)])
    rms /= n
    rms = sqrt(rms)
    rms /= (sum(expected)) / n

    # note: we probably won't use this as mbe suffers from "cancellation"
    # in other words (+ big error) + (- big error) = small error
    mbe = sum([(e - d) for (e, d) in zip(expected, data)])
    mbe /= sum(expected)
    return (rms, mbe)


def _schedule_edit(schedule, inner):
    """
    Helper function used by EpmHandler.create_schedule()
    Appends the actual schedule data (inner) to the existing `schedule`
    There are two ways of using this function:
    - inner is a list of tuples, of (time, data)
        e.g. (09:00, 0), (13:00, 1), (18:00, 0.5), (24:00, 0)
    - inner is simply a list of data
        in this case we separate the 24 hours into sections
        e.g. [0, 1, 0.5, 0], which is identical to the case above
    """
    if type(inner) is int or type(inner) is float:
        schedule.add_fields("Until: 24:00",
                            str(inner))
    if (type(inner[0]) is tuple) or (type(inner[0]) is list):
        for item in inner:
            schedule.add_fields(
                "Until: " + str(item[0]) + ',' + str(item[1]))
    else:
        assert(24 % len(inner) == 0)
        for i in range(len(inner)):
            schedule.add_fields(
                "Until: " + str((24 // len(inner)) * (i + 1)) + ":00",
                str(inner[i]))
    return schedule


class EpmHandler:
    """
    I created this wrapper so we can more easily change parameters and run simulations 
    in the future
    """

    def __init__(self, source_epm_path, epw_path):
        self.epm = op.Epm.from_idf(source_epm_path)
        self.epw = epw_path

    def simulate(self, name='test-simulation'):
        self.epm.save('tmp.idf')
        return op.simulate('tmp.idf', self.epw, name)

    def create_schedule(self, name, params):
        schedule = handler.epm.Schedule_Compact.add(
            name=name, schedule_type_limits_name="Any Number")
        if (type(params) is list) or (type(params) is tuple):
            schedule.add_fields(
                "Through: 12/31",
                "For: AllDays")
            schedule = _schedule_edit(schedule, params)
        elif type(params) is dict:
            schedule.add_fields(
                "Through: 12/31",
            )
            for key in params:
                schedule.add_fields("For: " + key)
                schedule = _schedule_edit(schedule, params[key])
        return schedule


eplus_dir_path = op.get_eplus_base_dir_path((9, 4, 0))
epw_path = os.path.join(
    "./hk_weather",
    "CHN_Hong.Kong.SAR.450070_CityUHK.epw"
)
handler = EpmHandler("test_input.idf", epw_path)

# print(handler.epm.ZoneHVAC_IdealLoadsAirSystem)
# print(handler.epm.ZoneHVAC_IdealLoadsAirSystem[0])
# print(handler.epm.ZoneHVAC_IdealLoadsAirSystem[0].name)
hvac_schedules = [handler.create_schedule("ZONE ONE HVAC Schedule 1",
                                          [0.0]),
                  handler.create_schedule("ZONE ONE HVAC Schedule 2",
                                          [1.0])
                  ]

people_schedules = [handler.create_schedule("ZONE ONE People Schedule 1",
                                            {'Weekdays': [("09:00", 0.0), ("15:00", 1.0), ("24:00", 0.0)], 'AllOtherDays': [0.0]}),
                    handler.create_schedule("ZONE ONE People Schedule 2",
                                            {'Weekdays': [("12:00", 0.0), ("20:00", 1.0), ("24:00", 0.0)], 'AllOtherDays': [0.0]})
                    ]


fig, axis = plt.subplots(2, 2)

# the first value in the first table named 'timestep'
#   (although we should only have one 'timestep table' that has only one value) @.@
timestep = handler.epm.timestep[0][0]

for people_style, people_schedule in zip(['b', 'g'], people_schedules):
    handler.epm.People[0].number_of_people_schedule_name = people_schedule.name
    for hvac_style, schedule in zip(['-', '-.', '--'], hvac_schedules + [people_schedule]):
        handler.epm.ZoneHVAC_IdealLoadsAirSystem[0].availability_schedule_name = schedule.name
        # handler.epm.ZoneHVAC_IdealLoadsAirSystem[0].heating_availability_schedule_name = schedule.name
        # handler.epm.ZoneHVAC_IdealLoadsAirSystem[0].cooling_availability_schedule_name = schedule.name
        s = handler.simulate('my-first-simulation')
        print(f"status: {s.get_status()}")
        print(f"Eplus .err file:\n{s.get_out_err().get_content()}")
        t1 = time()
        eso = s.get_out_eso()
        # print(eso.get_info())
        eso.create_datetime_index(2021)
        df = eso.get_data()
        t2 = time()
        print('grabbed data in', t2 - t1)
        for ax, start_day in zip([0, 1], [23, 7 * 30 + 23]):
            start_hour = start_day * 24 * timestep
            ax0 = axis[0][ax]
            ax0.plot(df[['zone one,Zone Air Temperature']]
                       [start_hour:start_hour + 24 * timestep], people_style + hvac_style, label=schedule.name)
            ax0.plot(df[['environment,Site Outdoor Air Drybulb Temperature']]
                       [start_hour:start_hour + 24 * timestep], 'r', label=schedule.name)

            # use this to check what is the name of the column:
            # (aka what is the string we need to use to index df)
            # print(df.keys())
            data = df[[
                'zone one people,Zone Thermal Comfort ASHRAE 55 Adaptive Model 80% Acceptability Status']][start_hour: start_hour + 24 * timestep]
            data2 = data.where(data >= 0.0)
            ax1 = axis[1][ax]
            ax1.plot(data2, people_style + hvac_style)
            ax1.set_xlim(data.index[0], data.index[-1])
        t3 = time()
        print('processed data in', t3 - t2)
plt.show()


"""for mat in epm.WindowMaterial_Glazing:
    if mat.name == 'clear 6mm':
        mat.conductivity = 0.1
for surface in epm.Material:
    if surface.name == 'fiberglass quilt-2':
        surface.conductivity = 0.001
        print(surface)
"""


# print(df)
# print(df.keys)
"""print(df[[
      'zone one people,Zone Thermal Comfort Pierce Model Effective Temperature PMV']])"""

# print(df[['zone one,Zone Air Temperature']][:24])
# print(dir(df[['zone one,Zone Air Temperature']][:24]))
